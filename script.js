const itemNameInput = document.getElementById('itemName');
const itemDescriptionInput = document.getElementById('itemDescription');
const itemPriceInput = document.getElementById('itemPrice');
const submitButton = document.getElementById('submitButton');
const content = document.getElementById("content")

fetch('data.json')
  .then(response => response.json())
  .then(data => {
    data.forEach(item => {
      const newFood = document.createElement('div');
      newFood.className = 'block';
      newFood.innerHTML = `<b>${item.name}</b><br>Apraksts: ${item.description}<br>Cena: ${item.price} €`;
      content.appendChild(newFood);
    });
  });

submitButton.addEventListener('click', function() {
  const itemName = itemNameInput.value;
  const itemDescription = itemDescriptionInput.value;
  const itemPrice = itemPriceInput.value;
  if (itemName.trim() !== '' && itemDescription.trim() !== '' && itemPrice.trim() !== '') {
    var newFood = document.createElement('div')
    newFood.className = "block"
    newFood.innerHTML = `<b>${itemName}</b><br>Apraksts: ${itemDescription}<br>Cena: ${itemPrice} €`;
    content.append(newFood)
  }
});