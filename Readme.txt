Vajag palaist lokālo serveri, lai vietne darbotos.
Vietnē var uzrakstīt ēdiena datus un pievienot to vietnes sarakstam.
Failā data.json var ierakstīt datu bāzi, kura tiks paradīta vietnē.

Dalībnieki:
Rīgas 71. vidusskolas 12 ab klases skolnieki

Egors Kusrajevs (gitlab: @yarikbarbaro) - frontend 
Kirils Tabačikovs (gitlab: @kiryl919191) - backend 
Alisija Bulgakova (gitlab: @bylgakkkova) - design and project manager
